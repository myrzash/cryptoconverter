//
//  RealmHelper.swift
//  CryptoConverter
//
//  Created by Make on 9/19/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation
import RealmSwift

class RealmHelper {
   
    func readQuotesFromStorage () -> [Quote]? {
        print("readQuotesFromStorage...")
        do {
            let realm = try Realm()
            return realm
                .objects(QuoteCached.self)
                .map{ Quote(quoteCached: $0) }
            
        } catch let error as NSError {
            print("ERROR: \(error.localizedDescription)")
        }
        return nil
    }
    
    func writeQuotesToStorage (quotes: [Quote]) {
        print("writeQuotesToStorage...")
        quotes
            .map{QuoteCached(quote: $0)}
            .forEach{ quote in
                do {
                    let realm = try Realm()
                    try realm.write ({
                        realm.add(quote, update: .modified)
                    })
                } catch let error as NSError {
                    print("ERROR: \(error.localizedDescription)")
                }
        }
    }
    
    func saveQuoteToStorage(quote: Quote, primaryKey: String) {
        let quoteCached = QuoteCached(quote: quote)
        quoteCached.name = primaryKey
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(quoteCached, update: .modified)
            }
        } catch let error as NSError {
            print("error: \(error)")
        }
    }
    
    func readQuoteFromStorage (primaryKey:String) -> Quote? {
        do {
            let realm = try Realm()
            if let quoteCached = realm.object(ofType: QuoteCached.self, forPrimaryKey: primaryKey) {
                return Quote(quoteCached: quoteCached)
            }
        } catch let error as NSError {
            print("ERROR: \(error.localizedDescription)")
        }
        return nil
    }
}
