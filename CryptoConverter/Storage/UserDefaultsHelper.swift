//
//  UserDefaultsHelper.swift
//  CryptoConverter
//
//  Created by Make on 9/19/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    
    private let KEY_OPENED_FIRST = "isFirstlyOpened"

    var isFirstOpened: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: KEY_OPENED_FIRST)
        }
        get {
            return UserDefaults.standard.bool(forKey: KEY_OPENED_FIRST)
        }
    }
}
