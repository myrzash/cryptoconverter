//
//  KeychainHelper.swift
//  CryptoConverter
//
//  Created by Make on 9/19/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation
import KeychainSwift

class KeychainHelper {
    
    private let KEY_USERPASSWORD = "userPassword"
    private let keychain = KeychainSwift()
    
    func saveUserPassword(password: String){
        guard keychain.set(password, forKey: KEY_USERPASSWORD) else {
            print("ERROR saveUserPassword")
            return
        }
    }
    
    func getUserPassword() -> String? {
        return keychain.get(KEY_USERPASSWORD)
    }
    
    func deleteUserPassword() {
        guard keychain.delete(KEY_USERPASSWORD) else {
            print("ERROR deleteUserPassword")
            return
        }
    }
    
    func clearAll(){
        keychain.clear()
    }
}
