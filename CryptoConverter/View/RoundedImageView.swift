//
//  RoundedImageView.swift
//  CryptoConverter
//
//  Created by Make on 9/23/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.masksToBounds = true
        layer.cornerRadius = bounds.height / 2
    }
    
}
