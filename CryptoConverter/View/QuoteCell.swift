//
//  QuoteCell.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/3/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit

class QuoteCell: UITableViewCell {
    
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var percentChange24hLabel: UILabel!
    @IBOutlet weak var priceUSDLabel: UILabel!
    @IBOutlet weak var availableSupplyLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
}
