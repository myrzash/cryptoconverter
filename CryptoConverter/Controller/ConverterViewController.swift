//
//  ConverterViewController.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/13/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit
import RealmSwift

class ConverterViewController: UIViewController {
    
    @IBOutlet weak var baseQuoteTextField: UITextField!
    @IBOutlet weak var observerTextField: UITextField!
    @IBOutlet weak var convertQuoteTextField: UITextField!
    
    @IBOutlet weak var baseQuoteIconButton: UIButton!
    @IBOutlet weak var baseQuoteSymbolLabel: UILabel!
    @IBOutlet weak var baseQuotePriceUSDLabel: UILabel!
    @IBOutlet weak var convertQuoteIconButton: UIButton!
    @IBOutlet weak var convertQuoteSymbolLabel: UILabel!
    @IBOutlet weak var convertQuotePriceUSDLabel: UILabel!
    
    static let NOTIFICATION_SELECTED_QUOTE = "selectedQuote"
    static let BUTTON_TAG = "buttonTagSelectQuote"
    
    enum KeyBoardKeys: Int {
        case clear = 10
        case delete = 11
        case divide = 12
        case replace = 13
        case multiply = 14
        case minus = 15
        case plus = 16
        case result = 17
        case dot = 18
    }
    
    private let calculator = Calculator()
    private let realmHelper: RealmHelper = RealmHelper()
    
    private var baseQuote: Quote? {
        set {
            realmHelper.saveQuoteToStorage(quote: newValue!, primaryKey: "baseQuote")
        }
        get {
            return realmHelper.readQuoteFromStorage(primaryKey: "baseQuote")
        }
    }
    
    private var convertQuote: Quote? {
        set {
            realmHelper.saveQuoteToStorage(quote: newValue!, primaryKey: "convertQuote")
        }
        get {
            return realmHelper.readQuoteFromStorage(primaryKey: "convertQuote")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(selectQuote),
                                               name: NSNotification.Name(ConverterViewController.NOTIFICATION_SELECTED_QUOTE),
                                               object: nil)
        setBaseQuote()
        setConvertQuote()
    }
    
    @IBAction func numberAction(_ sender: UIButton) {
        baseQuoteTextField.text? += String(sender.tag)
        calculate()
    }
    
    @IBAction func operatorAction(_ sender: UIButton) {
        
        if let tag = KeyBoardKeys(rawValue: sender.tag) {
            switch tag {
            case .divide:
                if let combination = baseQuoteTextField.text,
                    calculator.isValidToAddOperator(combination: combination) {
                    baseQuoteTextField.text?.append("÷")
                }
                
            case .multiply:
                if let combination = baseQuoteTextField.text,
                    calculator.isValidToAddOperator(combination: combination) {
                    baseQuoteTextField.text?.append("×")
                }
                
            case .minus:
                if let combination = baseQuoteTextField.text,
                    calculator.isValidToAddOperator(combination: combination) {
                    baseQuoteTextField.text?.append("-")
                }
                
            case .plus:
                if let combination = baseQuoteTextField.text,
                    calculator.isValidToAddOperator(combination: combination) {
                    baseQuoteTextField.text?.append("+")
                }
                
            case .dot:
                if let combination = baseQuoteTextField.text,
                    calculator.isValidToAddDot(combination: combination) {
                    baseQuoteTextField.text?.append(".")
                }
                
            case .result:
                baseQuoteTextField.text = observerTextField.text
                observerTextField.text = ""
                
            case .delete:
                if let str = baseQuoteTextField.text, str.count > 0 {
                    let index = str.index(str.endIndex, offsetBy: -1)
                    baseQuoteTextField?.text = String(str[..<index])
                    calculate()
                }
                
            case .replace:
                let buffer = baseQuote
                baseQuote = convertQuote
                convertQuote = buffer
                setBaseQuote()
                setConvertQuote()
                calculate()
                
            case .clear:
                baseQuoteTextField.text = ""
                observerTextField.text = ""
            }
        }
    }
    
    private func calculate () {
        
        if let text = baseQuoteTextField.text {
            if text.count == 0 {
                observerTextField.text = ""
                return
            } else if calculator.isEvaluatable(combination: text) {
                let amount = calculator.evaluate(combination: text)
                observerTextField.text = formatDouble(amount)
                if let baseQuote = self.baseQuote, let convertQuote = self.convertQuote {
                    let converter = Converter(baseQuote)
                    convertQuoteTextField.text = formatDouble(converter.calculate(amount: amount, convertQuote: convertQuote))
                }
            }
        }
    }
    
    private func formatDouble (_ n: Double) -> String {
        var str = String(format: "%.8f", n)
        
        if let regexRoundInt = str.range(of: #"[.]0*$"#, options: .regularExpression) {
            str.replaceSubrange(regexRoundInt, with: "")
        }
        if str.range(of: #"[.]\d+0*$"#, options: .regularExpression) != nil {
            str.replaceSubrange(str.range(of: #"0*$"#, options: .regularExpression)!, with: "")
        }
        return str
    }
    
    //    MARK: Views
    
    func setBaseQuote(){
        if let quote = self.baseQuote {
            let image = UIImage(named: quote.icon)
            baseQuoteIconButton.setImage(image, for: UIControl.State.normal)
            baseQuoteSymbolLabel.text = quote.symbol
            baseQuotePriceUSDLabel.text = String(format: "%.2f $", quote.priceUSD.toDouble())
        }
    }
    
    func setConvertQuote() {
        if let quote = self.convertQuote {
            let image = UIImage(named: quote.icon)
            convertQuoteIconButton.setImage(image, for: UIControl.State.normal)
            convertQuoteSymbolLabel.text = quote.symbol
            convertQuotePriceUSDLabel.text = String(format: "%.2f $", quote.priceUSD.toDouble())
        }
    }
    
    //     MARK: Notifications
    
    @objc func selectQuote(notif: Notification) {
        if let quote = notif.object as? Quote {
            if let buttonTag = notif.userInfo?[ConverterViewController.BUTTON_TAG] as? Int{
                
                switch buttonTag {
                case 1:
                    self.baseQuote = quote
                    setBaseQuote()
                case 2:
                    self.convertQuote = quote
                    setConvertQuote()
                default:
                    break
                }
            }
        }
    }
    
    //     MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let quotesTVC = segue.destination as? QuotesTableViewController {
            if let button = sender as? UIButton {
                quotesTVC.converterButtonTag = button.tag
            }
        }
    }
}
