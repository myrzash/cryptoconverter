//
//  QuoteDetailViewController.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/3/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit

class QuoteDetailViewController: UITableViewController {
    
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var priceUSDLabel: UILabel!
    @IBOutlet weak var priceBTCLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var marketCapLabel: UILabel!
    @IBOutlet weak var availableSupplyLabel: UILabel!
    @IBOutlet weak var maxSupplyLabel: UILabel!
    @IBOutlet weak var totalSupplyLabel: UILabel!
    @IBOutlet weak var percentChange1hLabel: UILabel!
    @IBOutlet weak var percentChange24hLabel: UILabel!
    @IBOutlet weak var percentChange7dLabel: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    var quote: Quote?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let quote = self.quote {
            self.title = quote.name
            navigationItem.titleView = navigationView(title: quote.name, icon: quote.icon, subTitle: quote.symbol)
            populateQuoteDatas(quote: quote)
        }
    }
    
    func populateQuoteDatas (quote: Quote) {
        rankLabel.text = String(quote.rank)
        priceUSDLabel.text = quote.priceUSD
        priceBTCLabel.text = quote.priceBTC
        volumeLabel.text = quote.volumeUsd24h
        
        marketCapLabel.text = String(format: "%.0f", quote.marketCapUSD.toDouble())
        availableSupplyLabel.text = String(format: "%.0f", quote.availableSupply.toDouble())
        maxSupplyLabel.text = String(format: "%.0f", quote.maxSupply?.toDouble() ?? "0")
        totalSupplyLabel.text = String(format: "%.0f", quote.totalSupply.toDouble())
        percentChange24hLabel.text = String(format: "%.2f", quote.percentChange24h.toDouble()) + "%"
        percentChange1hLabel.text = String(format: "%.2f", quote.percentChange1h.toDouble()) + "%"
        percentChange7dLabel.text = String(format: "%.2f", quote.percentChange7d.toDouble()) + "%"
        lastUpdatedLabel.text = dateToString(quote.lastUpdated.toDouble())
        
        percentChange24hLabel.textColor = quote.getColorPercentChange24h()
        percentChange1hLabel.textColor = quote.getColorPercentChange1h()
        percentChange7dLabel.textColor = quote.getColorPercentChange7d()
        
    }
    
    func dateToString(_ timeIntervalSince1970: Double) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "dd/MM/YYYY hh:mm a"
        let date = Date(timeIntervalSince1970: timeIntervalSince1970)
        return dateFormatter.string(from: date)
    }
    
    
    func navigationView(title: String, icon: String, subTitle: String) -> UIView {
        let titleView = UIView()
        let marginElements:CGFloat = 10
        //        Quote Name
        let label = UILabel()
        label.text = title
        label.sizeToFit()
        label.center = titleView.center
        label.textAlignment = NSTextAlignment.center
        titleView.addSubview(label)
        
        //        Quote symbol
        let subLabel = UILabel()
        subLabel.text = subTitle
        subLabel.textColor = UIColor.gray
        subLabel.sizeToFit()
        subLabel.frame = CGRect(
            x: label.frame.origin.x + label.frame.size.width + marginElements,
            y: label.frame.origin.y,
            width: 100,
            height: label.frame.size.height
        )
        titleView.addSubview(subLabel)
        
        //        Quote icon
        if let image = UIImage(named: icon) {
            let imageView = UIImageView()
            imageView.image = image
            let imageAspect = imageView.image!.size.width / imageView.image!.size.height
            imageView.frame = CGRect(
                x: label.frame.origin.x - label.frame.size.height * imageAspect - marginElements,
                y: label.frame.origin.y,
                width: label.frame.size.height * imageAspect,
                height: label.frame.size.height
            )
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            titleView.addSubview(imageView)
        }
        
        titleView.sizeToFit()
        return titleView
    }
}
