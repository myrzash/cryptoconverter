//
//  QuotesTableViewController.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/3/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import AnimatableReload
import UIKit

class QuotesTableViewController: UITableViewController {
    
    private var quoteProvider = QuoteProvider()
    private var quotes = [Quote]()
    private let realmHelper = RealmHelper()
    var converterButtonTag:Int?
    private var isAnimatedShowList:Bool = false
    private var loadIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.topItem?.title = " "
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retrieveQuotes),
                                               name: Notification.Name(QuoteProvider.NOTIFICATION_QUOTES),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onError),
                                               name: Notification.Name(QuoteProvider.ERROR_CONNECTION),
                                               object: nil)
        
        loadIndicator = UIActivityIndicatorView()
        loadIndicator!.frame = CGRect(x: 0.0,y: 0.0,width: 40.0,height: 40.0);
        loadIndicator!.center = self.view.center
        loadIndicator!.hidesWhenStopped = true
        loadIndicator!.style = UIActivityIndicatorView.Style.gray
        self.view.addSubview(loadIndicator!)
        
        // TODO: Realm. Get data from cache
        if let quotes = realmHelper.readQuotesFromStorage(), quotes.count != 0 {
            self.quotes = quotes
            AnimatableReload.reload(tableView: tableView, animationDirection: "right")
        } else {
            showLoading()
            quoteProvider.loadQuotes()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        quoteProvider.retreiveQuotesByTimeInterval(seconds: 2*60)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        quoteProvider.stopRetrieveQuotesByTimeInterval()
    }
    
    // MARK: - Actions
    
    @IBAction func refreshQuotesAction(_ sender: UIBarButtonItem) {
        showLoading()
        quoteProvider.loadQuotes()
    }
    
    private func showLoading(){
        isAnimatedShowList = true
        loadIndicator?.startAnimating()
    }
    
    private func hideLoading () {
        loadIndicator?.stopAnimating()
        loadIndicator?.hidesWhenStopped = true
        isAnimatedShowList = false
    }
    // MARK: - Notifications
    
    @objc func onError(notif: Notification){
        if let message = notif.object as? String {
            let alertController = UIAlertController(title: "Ошибка подключения", message: message, preferredStyle: .alert)
            
            let refreshAction = UIAlertAction(title: "Повторить", style: .default, handler: {action in
                self.quoteProvider.loadQuotes()})
            alertController.addAction(refreshAction)
            
            let closeAction = UIAlertAction(title: "Закрыть", style: .default, handler: nil)
            alertController.addAction(closeAction)
            
            hideLoading()
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func retrieveQuotes(notification: Notification){
        if let quotes = notification.object as? [Quote] {
            realmHelper.writeQuotesToStorage(quotes: quotes)
            self.quotes = quotes
            if isAnimatedShowList {
                AnimatableReload.reload(tableView: tableView, animationDirection: "right")
                hideLoading()
            } else {
                tableView.reloadData()
            }
        }
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quotes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let quote = quotes[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "QuoteCell", for: indexPath) as? QuoteCell {
            cell.rankLabel.text = String(quote.rank)
            cell.nameLabel.text = quote.name
            cell.symbolLabel.text = quote.symbol
            cell.availableSupplyLabel.text = "Supply \(formatMoney(quote.availableSupply))"//
            cell.priceUSDLabel.text = String(format: "%.2f $", quote.priceUSD.toDouble())
            cell.percentChange24hLabel.text = String(format: "%.2f", quote.percentChange24h.toDouble()) + " %"
            cell.percentChange24hLabel.textColor = quote.getColorPercentChange24h()
            
            if let logo = UIImage(named: quote.icon) {
                cell.iconImageView.image = logo
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let quote = quotes[indexPath.row]
        NotificationCenter.default.post(name: NSNotification.Name(ConverterViewController.NOTIFICATION_SELECTED_QUOTE), object: quote, userInfo: [ConverterViewController.BUTTON_TAG: converterButtonTag ?? 0])
        dismiss(animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return converterButtonTag == nil
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "quotesListToDetail" {
            
            if let cell = sender as? QuoteCell,
                let indexPath = tableView.indexPath(for: cell) {
                let quote = quotes[indexPath.row]
                let detailVC = segue.destination as? QuoteDetailViewController
                detailVC?.quote = quote
            }
        }
    }
    
    // MARK: - Storage Examples
    //    TODO: Works with Keychain
    func exampleKeychain () {
        let keychainHelper = KeychainHelper()
        if let userPassword = keychainHelper.getUserPassword() {
            print("Keychain user password: \(userPassword)")
        } else {
            print("Keychain user password NOT FOUND")
            keychainHelper.saveUserPassword(password: "123456")
        }
    }
    
    //    TODO: Works with UserDefaults
    func showAlertWhenFirstLogin (message: String = "Welcome to CryptoConverter!") {
        let userDefaultsHelper = UserDefaultsHelper()
        if !userDefaultsHelper.isFirstOpened {
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "go", style: .default, handler:{action in
                userDefaultsHelper.isFirstOpened = true
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func formatMoney(_ money: String) -> String {
        switch money.count {
        case 6...: return String(format: "%.2f M", money.toDouble()/1000000)
        case 3...: return String(format: "%.2f K", money.toDouble()/1000)
        default: return money
        }
    }
}
