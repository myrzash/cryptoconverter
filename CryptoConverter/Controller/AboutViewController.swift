//
//  AboutViewController.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/13/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit
import SwiftySound

class AboutViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    private var backgroundSound: Sound?
    private var autoScroll: AutoScroll?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
            appNameLabel.text = appName
            appVersionLabel.text = "Version \(appVersion)"
        }
        
        if let musicUrl = Bundle.main.url(forResource: "game_of_thrones", withExtension: "wav") {
            backgroundSound = Sound(url: musicUrl)
            backgroundSound?.prepare()
            print("prepare sound")
        }
        
        let countPage:CGFloat = 4
        let totalPossibleOffset = countPage * self.view.bounds.size.height
        autoScroll = AutoScroll(scrollView: self.scrollView, totalPossibleOffset: totalPossibleOffset)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        pauseSoundBg()
        autoScroll?.stop()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        playSoundBg()
        autoScroll?.start()
    }
    
    func pauseSoundBg() {
        backgroundSound?.pause()
    }
    
    func playSoundBg(){
        guard let backgroundSound = backgroundSound else { return }
        if !backgroundSound.resume() {
            backgroundSound.play(numberOfLoops: -1)
        }
    }
    
    //    MARK: Old view animations
    //    func animateAppnameAndVersion(){
    //        appNameLabel.alpha = 0
    //        appVersionLabel.alpha = 0
    //        UIView.animate(withDuration: animationDuration,
    //                       animations: {
    //
    //                        self.appNameLabel.frame = CGRect(origin: CGPoint(x: self.appNameLabel.center.x, y:0), size: self.appNameLabel.frame.size)
    //
    //                        self.appVersionLabel.frame = CGRect(origin: CGPoint(x: self.appVersionLabel.center.x, y:0), size: self.appVersionLabel.frame.size)
    //
    //                        self.appNameLabel.alpha = 1
    //                        self.appVersionLabel.alpha = 1
    //        })
    //    }
    //
    //    func animateEmail(){
    //        emailLabel.alpha = 0
    //        UIView.animate(
    //            withDuration: animationDuration,
    //            delay: 0.1,
    //            options: [.curveEaseIn],
    //            animations: {
    //                self.emailLabel.frame = CGRect(origin: CGPoint(x: 0, y:-self.emailLabel.center.y), size: self.emailLabel.frame.size)
    //                self.emailLabel.alpha = 1
    //        })
    //    }
    //
    //    func animateFullname(){
    //        fullNameLabel.alpha = 0
    //        UIView.animate(
    //            withDuration: animationDuration,
    //            delay: 0,
    //            options: [.curveEaseInOut],
    //            animations: {
    //                self.fullNameLabel.frame = CGRect(origin: CGPoint(x: 0, y:-self.fullNameLabel.center.y), size: self.fullNameLabel.frame.size)
    //                self.fullNameLabel.alpha = 1
    //        })
    //    }
    //
    //    func animateAvatar(){
    //        if let avatarImage = self.avatarImageView {
    //            avatarImage.alpha = 0
    //            UIView.animate(
    //                withDuration: animationDuration,
    //                delay: 0.2,
    //                options: [.curveEaseInOut],
    //                animations: {
    //                    avatarImage.alpha = 1
    //            })
    //        }
    //    }
}
