//
//  Quote.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/3/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation
import UIKit

class Quote: Decodable {
    
    var id: String = ""
    var name: String = ""
    var symbol: String = ""
    var rank: String = ""
    var priceUSD: String = ""
    var priceBTC: String = ""
    var volumeUsd24h: String = ""
    var marketCapUSD: String = ""
    var availableSupply: String = ""
    var totalSupply: String = ""
    var maxSupply: String? = ""
    var percentChange1h: String = ""
    var percentChange24h: String = ""
    var percentChange7d: String = ""
    var lastUpdated: String = ""
    var icon: String {
        get {
            return "\(self.id).png"
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case symbol
        case rank
        case priceUSD = "price_usd"
        case priceBTC = "price_btc"
        case volumeUsd24h = "24h_volume_usd"
        case marketCapUSD = "market_cap_usd"
        case availableSupply = "available_supply"
        case totalSupply = "total_supply"
        case maxSupply = "max_supply"
        case percentChange1h = "percent_change_1h"
        case percentChange24h = "percent_change_24h"
        case percentChange7d = "percent_change_7d"
        case lastUpdated = "last_updated"
    }
    
    init(quoteCached: QuoteCached) {
        id = quoteCached.id
        name = quoteCached.name
        symbol = quoteCached.symbol
        rank = quoteCached.rank
        priceUSD = quoteCached.priceUSD
        priceBTC = quoteCached.priceBTC
        volumeUsd24h = quoteCached.volumeUsd24h
        marketCapUSD = quoteCached.marketCapUSD
        availableSupply = quoteCached.availableSupply
        totalSupply = quoteCached.totalSupply
        maxSupply = quoteCached.maxSupply
        percentChange1h = quoteCached.percentChange1h
        percentChange24h = quoteCached.percentChange24h
        percentChange7d = quoteCached.percentChange7d
        lastUpdated = quoteCached.lastUpdated
    }
    
    func getColorPercentChange24h () -> UIColor {
        return getColorText(percent: self.percentChange24h.toDouble())
    }
    
    func getColorPercentChange1h () -> UIColor {
        return getColorText(percent: self.percentChange1h.toDouble())
    }
    
    func getColorPercentChange7d () -> UIColor {
        return getColorText(percent: self.percentChange7d.toDouble())
    }
    
    private func getColorText (percent: Double) -> UIColor {
        return percent > 0 ? UIColor.green : UIColor.red
    }
}

extension String {
    func toDouble () -> Double {
        return Double(self) ?? 0
    }
    
    func toInt () -> Int {
        return Int(self) ?? 0
    }
}
