//
//  Converter.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 8/27/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation

class Converter {
    var baseQuote: Quote
    
    init(_ baseQuote: Quote) {
        self.baseQuote = baseQuote
    }
    
    func calculate(amount: Double, convertQuote: Quote) -> Double {
        return amount * convertQuote.priceUSD.toDouble() / self.baseQuote.priceUSD.toDouble()
    }
}
