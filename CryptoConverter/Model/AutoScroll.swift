//
//  AutoScroll.swift
//  CryptoConverter
//
//  Created by Make on 9/27/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import UIKit
import Foundation

class AutoScroll {
    
    private var scrollDuration:TimeInterval
    private var animator: UIViewPropertyAnimator?
    private var scrollView: UIScrollView?
    private var totalPossibleOffset: CGFloat?
    
    init (scrollView: UIScrollView, totalPossibleOffset: CGFloat, scrollDuration: TimeInterval = 15) {
        self.scrollView = scrollView
        self.totalPossibleOffset = totalPossibleOffset
        self.scrollDuration = scrollDuration
    }
    
    func start(){
        if let scrollView = self.scrollView, let totalPossibleOffset = self.totalPossibleOffset {
            scrollView.contentOffset.y = 0
            let coefficient = 1 - scrollView.contentOffset.y / totalPossibleOffset
            let duration = scrollDuration * Double(coefficient)
            
            animator = UIViewPropertyAnimator(duration: duration, curve: .linear, animations: {
                scrollView.contentOffset.y = totalPossibleOffset
            })
            animator?.isUserInteractionEnabled = false
            animator?.addCompletion { completed in
                self.start()
            }
            animator?.startAnimation()
        }
    }
    
    func stop () {
        animator?.stopAnimation(true)
    }
}
