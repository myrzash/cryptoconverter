//
//  Calculator.swift
//  CryptoConverter
//
//  Created by Make on 9/27/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation

class Calculator {
    
    private let regexLastOperator = #"[+×÷\-]$"#
    
    func isValidToAddDot (combination: String) -> Bool {
        let isDoubleDot = combination.range(of: #"[.]\d*$"#, options: .regularExpression) != nil
        let isLastOperator = combination.range(of: regexLastOperator, options: .regularExpression) != nil
        
        return combination != "" && !isDoubleDot && !isLastOperator
    }
    
    func isValidToAddOperator(combination: String) -> Bool {
        let isLastOperator = combination.range(of: regexLastOperator, options: .regularExpression)  != nil
        let isLastDot = combination.range(of: #"[.]$"#, options: .regularExpression) != nil
        
        return combination != "" && !isLastOperator && !isLastDot
    }
    
    func evaluate(combination: String) -> Double {
        let normalizedString = normalizeEval(combination: combination)
        let expression = NSExpression(format: normalizedString)
        let result = expression.expressionValue(with: nil, context: nil) as? Double
        return result ?? 0
    }
    
    private func normalizeEval(combination: String) -> String {
        return combination
            .replacingOccurrences(of: "×", with: "*")
            .replacingOccurrences(of: "÷", with: "/")
    }
    
   func isEvaluatable (combination: String) -> Bool {
      let isLastOperator = combination.range(of: regexLastOperator, options: .regularExpression) != nil
      let isLastDot = combination.range(of: #"[.]$"#, options: .regularExpression) != nil
      
      let isLastSymbol = isLastOperator || isLastDot
      return combination.count != 0 && !isLastSymbol
   }
}
