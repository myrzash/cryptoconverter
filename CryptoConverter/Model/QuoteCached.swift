//
//  QuoteCached.swift
//  CryptoConverter
//
//  Created by Make on 9/19/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation
import RealmSwift

class QuoteCached: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var symbol: String = ""
    @objc dynamic var rank: String = ""
    @objc dynamic var priceUSD: String = ""
    @objc dynamic var priceBTC: String = ""
    @objc dynamic var volumeUsd24h: String = ""
    @objc dynamic var marketCapUSD: String = ""
    @objc dynamic var availableSupply: String = ""
    @objc dynamic var totalSupply: String = ""
    @objc dynamic var maxSupply: String? = ""
    @objc dynamic var percentChange1h: String = ""
    @objc dynamic var percentChange24h: String = ""
    @objc dynamic var percentChange7d: String = ""
    @objc dynamic var lastUpdated: String = ""
    @objc dynamic var icon: String = ""
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    convenience init (quote: Quote) {
        self.init()
        id = quote.id
        icon = quote.icon
        name = quote.name
        symbol = quote.symbol
        rank = quote.rank
        priceUSD = quote.priceUSD
        priceBTC = quote.priceBTC
        volumeUsd24h = quote.volumeUsd24h
        marketCapUSD = quote.marketCapUSD
        availableSupply = quote.availableSupply
        totalSupply = quote.totalSupply
        maxSupply = quote.maxSupply
        percentChange1h = quote.percentChange1h
        percentChange24h = quote.percentChange24h
        percentChange7d = quote.percentChange7d
        lastUpdated = quote.lastUpdated
    }
}
