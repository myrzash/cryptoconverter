//
//  QuoteProvider.swift
//  CryptoConverter
//
//  Created by Kozhakhmet Myrzagali on 9/3/19.
//  Copyright © 2019 Kozhakhmet Myrzagali. All rights reserved.
//

import Foundation

class QuoteProvider {
    
    static let NOTIFICATION_QUOTES = "quotesUpdateNotification"
    static let ERROR_CONNECTION = "quotesErrorConnection"
    //    static let URL_API_QUOTES = "http://127.0.0.1/data.json"
    static let URL_API_QUOTES = "https://api.coinmarketcap.com/v1/ticker/"
    private var timer:Timer?
    
    func retreiveQuotesByTimeInterval (seconds: Int = 5*60) {
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(seconds),
                             target: self,
                             selector: #selector(loadQuotes),
                             userInfo: nil,
                             repeats: true)
    }
    
    func stopRetrieveQuotesByTimeInterval(){
        timer?.invalidate()
        timer = nil
    }
    
    @objc func loadQuotes () {
        guard let url = URL(string: QuoteProvider.URL_API_QUOTES) else {
            print("Ошибка в строке URL")
            return
        }
        
        let taskLoadingQuotes = URLSession.shared.dataTask(with: url) {
            (data, responce, error) in
            
            guard data != nil else {
                NotificationCenter.default.post(name: Notification.Name(QuoteProvider.ERROR_CONNECTION), object: "Проверьте подключение к сети")
                return
            }
            
            if let data = data,
                let quotes = try? JSONDecoder().decode([Quote].self, from: data) {
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name(QuoteProvider.NOTIFICATION_QUOTES),
                                                    object: quotes)
                }
                
            }
        }
        taskLoadingQuotes.resume()
    }
}
